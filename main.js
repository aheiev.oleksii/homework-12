let sports = [
  ["skier", "⛷"],
  ["snowboarder", "🏂"],
  ["apple", "🍎"],
  ["hockey", "🏒"],
  ["ice skate", "⛸"],
  ["swimmer", "🏊"],
  ["surfer", "🏄‍"],
  ["watermelon", "🍉"],
  ["lemon", "🍋"],
  ["rowboat", "🚣"],
  ["bicyclist", "🚴‍"],
];

let winterSports = sports.slice(0, 5);
let summerSports = sports.slice(5);
let fruits = [];

for (let i = 0; i < winterSports.length; i++) {
  for (let j = 0; j < winterSports[i].length; j++) {
    if (winterSports[i][j] === "apple") {
      fruits.push(winterSports[i]);
    }
  }
}

for (let i = 0; i < summerSports.length; i++) {
  for (let j = 0; j < summerSports[i].length; j++) {
    if (summerSports[i][j] === "lemon" || summerSports[i][j] === "watermelon") {
      fruits.push(summerSports[i]);
    }
  }
}

winterSports.splice(2, 1);
summerSports.splice(2, 2);

for (let i = 0; i < winterSports.length; i++) {
  if (winterSports[i][0] === "skier") {
    console.log("*** Winter sports ***");
  }
  console.log(winterSports[i].join(": "));
}

for (let i = 0; i < summerSports.length; i++) {
  if (summerSports[i][0] === "swimmer") {
    console.log("*** Summer sports ***");
  }
  console.log(summerSports[i].join(": "));
}

for (let i = 0; i < fruits.length; i++) {
  if (fruits[i][0] === "apple") {
    console.log("*** Fruits ***");
  }
  console.log(fruits[i].join(": "));
}
